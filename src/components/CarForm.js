import React, { Component } from 'react';

import { required, maxLength, validate } from '../lib/validate';

const CAR_FIELDS = [
  {
    label: 'Name',
    type: 'text',
    name: 'name',
    validators: [required, maxLength(200)]
  },
  {
    label: 'Acceleration',
    type: 'number',
    name: 'acceleration',
    validators: [required]
  },
  {
    label: 'Displacement',
    type: 'number',
    name: 'displacement',
    validators: [required]
  },
  {
    label: 'Gorsepower',
    type: 'number',
    name: 'horsepower',
    validators: [required]
  },
  {
    label: 'MpG',
    type: 'number',
    name: 'mpg',
    validators: [required]
  },
  {
    label: 'Weight',
    type: 'number',
    name: 'weight',
    validators: [required]
  },
  {
    label: 'Year',
    type: 'number',
    name: 'year',
    validators: [required]
  }
];

export default class CarForm extends Component {
  state = {
    model: Object.assign({}, this.props.car),
    errors: {}
  };

  handleInputChange = ({ target: { name, value } }) => {
    const validators = CAR_FIELDS.find(field => field.name === name).validators;
    const errors = validate(name, value, validators);

    this.setState({
      model: Object.assign({}, this.state.model, { [name]: value }),
      errors: Object.assign({}, this.state.errors, errors)
    });
  };

  handleFormSubmit = e => {
    e.preventDefault();
    this.props.onSubmit(this.state.model);
  };

  renderErrors = name => {
    if (this.state.errors[name]) {
      return (
        <span className="helper-text white-text red">
          {this.state.errors[name].message}
          <br />
        </span>
      );
    }
  };

  renderFields = () => {
    return CAR_FIELDS.map(({ label, type, name }) => (
      <label key={name}>
        {label}
        <input
          type={type}
          value={this.state.model[name]}
          onChange={this.handleInputChange}
          name={name}
        />
        {this.renderErrors(name)}
      </label>
    ));
  };

  render() {
    return (
      <form onSubmit={this.handleFormSubmit}>
        {this.renderFields()}

        <div className="card-action">
          <button
            className="waves-effect waves-light btn"
            disabled={Object.values(this.state.errors).some(val => !!val)}
            type="submit"
          >
            Submit
          </button>
          <button
            className="waves-effect waves-light btn red"
            onClick={this.props.onCancel}
          >
            Cancel
          </button>
        </div>
      </form>
    );
  }
}
