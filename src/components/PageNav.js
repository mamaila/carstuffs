import React, { Component } from 'react';

export default class PageNav extends Component {
  render() {
    return (
      <ul id="nav-mobile" className="right">
        <li>
          <button
            className="waves-effect waves-light btn"
            style={{ marginRight: '20px' }}
            onClick={this.props.onPreviousPage}
          >
            Previous
          </button>
        </li>
        <li>
          <button
            className="waves-effect waves-light btn"
            style={{ marginRight: '20px' }}
            onClick={this.props.onNextPage}
          >
            Next
          </button>
        </li>
        <li style={{ marginRight: '20px' }}>
          Page {this.props.page} of {this.props.maxPage}
        </li>
      </ul>
    );
  }
}
