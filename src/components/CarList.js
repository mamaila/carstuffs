import React, { Component } from 'react';

import GridList from './GridList';
import CarForm from './CarForm';

import myLogger from '../lib/logger';
import { action, actionLogger } from '../lib/action';

const log = myLogger('CarsList Component');
const carActionLogger = actionLogger(log);
const carAction = action(carActionLogger);

export default class CarList extends Component {
  state = {
    edit: null
  };

  userClicksEditCar = carAction(car => {
    this.setState({ edit: car.id });
  }, 'USER_CLICKS_EDIT');

  userClicksCancelEditCar = carAction(car => {
    this.setState({ edit: null });
  }, 'USER_CLICKS_CANCEL_EDIT');

  userClicksSubmitEditCar = carAction(async car => {
    this.props.onUpdateCar(car);
    this.setState({ edit: null });
  }, 'USER_CLICKS_SUBMIT_FORM');

  renderReadOnly = car => {
    return (
      <div className="card blue-grey darken-1">
        <div className="card-content white-text">
          <span className="card-title">{car.name}</span>
          <ul>
            <li>Acceleration: {car.acceleration}</li>
            <li>Displacement: {car.displacement}</li>
            <li>Horsepower: {car.horsepower}</li>
            <li>MpG: {car.mpg}</li>
            <li>Weight: {car.weight}</li>
            <li>Year: {car.year}</li>
          </ul>
        </div>
        <div className="card-action">
          <button
            className="waves-effect waves-light btn"
            onClick={() => {
              this.userClicksEditCar(car);
            }}
          >
            Edit
          </button>
        </div>
      </div>
    );
  };

  renderEditForm = car => {
    return (
      <div className="card blue-grey darken-1">
        <div className="card-content white-text">
          <CarForm
            car={car}
            onSubmit={this.userClicksSubmitEditCar}
            onCancel={this.userClicksCancelEditCar}
          />
        </div>
      </div>
    );
  };

  renderCarCard = car => {
    const renderCar =
      car.id === this.state.edit ? this.renderEditForm : this.renderReadOnly;

    return (
      <div className="col s12 m6 l6" key={car.id}>
        {renderCar(car)}
      </div>
    );
  };

  render() {
    return (
      <GridList
        list={this.props.cars}
        rowSize={2}
        renderItem={this.renderCarCard}
      />
    );
  }
}
