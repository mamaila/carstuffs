import React, { Component } from 'react';
import PageNav from './PageNav';
import CarList from './CarList';

import myLogger from '../lib/logger';
import { action, actionLogger } from '../lib/action';
import fetchCars from '../lib/fetchCars';
import updateCar from '../lib/updateCar';

const log = myLogger('App Component');
const appActionLogger = actionLogger(log);
const appAction = action(appActionLogger);

export default class App extends Component {
  state = {
    cars: [],
    page: 1,
    perPage: 20,
    maxPage: 1,
    message: null
  };

  componentDidMount() {
    this.userOpensApp();
  }

  getCars = async () => {
    try {
      const cars = await fetchCars(this.state.page, this.state.perPage);
      this.setState({
        cars: cars.objects,
        maxPage: Math.floor(cars.meta.total_count / this.state.perPage) + 1
      });
    } catch (e) {
      log('Error', e.message);
      this.setState({ message: 'Unable to fetch cars' });
    }
  };

  goToPage = page => {
    if (1 <= page && page <= this.state.maxPage) {
      this.setState({ page }, this.getCars);
    }
  };

  onUpdateCar = async car => {
    try {
      const newCar = await updateCar(car);
      log('new car', newCar);
      this.setState({
        cars: this.state.cars.map(car => {
          return car.id === newCar.id ? newCar : car;
        }),
        message: 'Update succesful'
      });
    } catch (e) {
      log('Error', e.message);
      this.setState({ message: 'Update failed' });
    }
  };

  userOpensApp = appAction(this.getCars, 'USER_OPENS_APP');

  userClicksNextPage = appAction(() => {
    this.goToPage(this.state.page + 1);
  }, 'USER_CLICKS_NEXT_PAGE');

  userClicksPreviousPage = appAction(() => {
    this.goToPage(this.state.page - 1);
  }, 'USER_CLICKS_NEXT_PAGE');

  render() {
    return (
      <div>
        <div className="navbar-fixed">
          <nav>
            <div className="container">
              <span className="center-align">{this.state.message}</span>
              <PageNav
                onPreviousPage={this.userClicksPreviousPage}
                onNextPage={this.userClicksNextPage}
                page={this.state.page}
                maxPage={this.state.maxPage}
              />
            </div>
          </nav>
        </div>
        <div className="container">
          <CarList cars={this.state.cars} onUpdateCar={this.onUpdateCar} />
        </div>
      </div>
    );
  }
}
