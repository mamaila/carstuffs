export const action = (...sideEffects) => (fn, name) => {
  return function(...args) {
    sideEffects.forEach(sideEffect => {
      sideEffect.call(this, fn, name, args);
    });

    return fn.apply(this, args);
  };
};

export const actionLogger = logger => (fn, name, args) => {
  logger(`Calling action ${name || fn.name} with args`, args);
};
