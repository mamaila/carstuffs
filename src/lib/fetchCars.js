const fetchCars = async (page, perPage) => {
  const offset = (page - 1) * perPage;
  const res = await fetch(
    `/api/v1/car/?format=json&limit=${perPage}&offset=${offset}`
  );

  if (res.ok) {
    const json = await res.json();
    return json;
  }

  throw new Error('Error fetching cars');
};

export default fetchCars;
