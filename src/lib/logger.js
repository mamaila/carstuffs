const getLogger = namespace => (...args) => {
  console.log(`[${new Date().toISOString()}] ${namespace}`);
  console.log.apply(this, args);
};

export default getLogger;
