export const maxLength = length => input => {
  if (input.length > length) {
    return {
      error: true,
      message: `This field has to be at most ${length} characters long.`
    };
  }
  return { error: false };
};

export const minLength = length => input => {
  if (input.length < length) {
    return {
      error: true,
      message: `This field has to be at least ${length} characters long.`
    };
  }
  return { error: false };
};

export const required = minLength(1);

export const validate = (name, value, validators) => {
  const errors = { [name]: null };
  const validatorsLength = validators.length;
  for (let i = 0; i < validatorsLength; i++) {
    const validatorObj = validators[i](value);
    if (validatorObj.error) {
      errors[name] = validatorObj;
      break;
    }
  }

  return errors;
};
