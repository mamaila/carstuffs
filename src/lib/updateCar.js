const updateCar = async car => {
  const res = await fetch(`/api/v1/car/${car.id}/?format=json`, {
    method: 'PUT',
    body: JSON.stringify(car),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  });

  if (res.ok) {
    const json = await res.json();
    return json;
  }

  throw new Error('Error updating car');
};

export default updateCar;
